# K8S 
*kubernetes*

Desde una punto de vista muy general , los componentes de un cluster de kubernetes son:

- **Nodos master** *(parte del control-plane)*
- **Nodos workers**

![components-of-kubernetes.svg](./res/components-of-kubernetes.svg)

<span style="color:#0a95ff">El nodo master</span> es el entorno dónde corre el **control-plane**, que es el cerebreo detrás de las operaciones que suceden dentro del cluster.  Dicho lo anterior, es evidente que hay que mantener este componente arriba a toda costa.  Para esto, se puede agregar replicas del nodo master, manteniendo un solo nodo en funcionamiento, pero manteniendo sincronizandos al resto de replicas.  


<span style="color:#0a95ff">El nodo worker</span> Es el encargado de instanciar cada recurso del cluster como pods, servicios, ingress, secrets, etc...


---
## 🧠 Componetes control-plane
<br> 

- <span style="color:#e28743">**Api-Server**</span>  
Intercepta las llamadas RESTFull, las valida y las procesa, también es el único componente que puede leer y escribir en la base de datos del <span style="color:#e28743">Data Store</span>. Funciona como una interface para cualquier componente del **control-plane** que requiera saber el estado del cluster.   
Este componente puede ser escalado horizontalmente, sin embargo soporta la adición de Api-Servers customizados, transformando al Api-Server principal como un punto de entrada proxy hacia los secundarios.

- <span style="color:#e28743">**Scheduler**</span>  
El objetivo de este componente es asignar a dónde se instanciaran los recursos dentro del cluster. Para esto, se apoya del <span style="color:#e28743">Api-Server</span> con el fin de **saber qué recurso y en qué nodo existe** así como para **obtener los requerimientos del nuevo objeto** a crear. El resultado de la desisión que toma el Scheduler, es comunicada al <span style="color:#e28743">Api-Server</span> para que éste delegue la instanciación de los recursos a otros componentes del **control-plane**.  
Este componente es customizable y configurable por medio de políticas, plugins y profiles, sin embargo se pueden agregar Schedulers cutomizados específicos para cada tipo de recurso.

- <span style="color:#e28743">**Control Manager**</span>  
Este componente regula el estado del cluster. Constantemente está verificando que lo que exista en el cluster(información obtenida por objectos de configuración), corresponda a lo que está descrito en la base de datos del <span style="color:#e28743">Data Store</span> (información que obtiene por medio del <span style="color:#e28743">Api-Server</span>). En caso de que no se cumpla ese requerimiento, se tomarán acciones en el cluster para que sea de esa forma.
  
  - kube-controller-manager.  
  Ejecuta controladores responsables de actuar cuando se pierden nodos en el cluster para garantizar que existan los pods, service accounts, endpoints y API access tokens requeridos.
  - cloud-controller-manager.  
  Ejecuta controladores responsables de interactuar con un proveedor de nube cuando un nodo deja de estar disponible para manejar volúmenes de almacenamiento si es que estos son provistos por un servicio de nube, maneja también el balanceo de cargas y el routeo.  


- <span style="color:#e28743">**Data Store**</span>  
Permite mantener un estado deseado del cluster. Se basa generalmente en un a base de datos **etcd** aunque existen clusters de pruebas que pueden usar otro tipo como **slqite**.  
Dentro del clutser, solo el <span style="color:#e28743">Api-Server</span> puede tener comunicación con el <span style="color:#e28743">Data Store</span> 
  - etcd  
  Es una base de datos consistente y de tipo llave-valor que solo implementa la adición de valores, la información nunca se reemplaza. Para minimizar el efecto en espacio, constantemente comprime la información obsoleta.   
  [Etcdctl](https://github.com/etcd-io/etcd/tree/main/etcdctl) es un cli que permite hacer backups, snapshots y recuperar capacidades que son úiles manejando una sola instancia de etcd en un cluster, sin embargo para poder tener un cluster productivo, la mejor alternativa es replicar la data en modo de alta disponibilidad(**HA**)
---
## 👷 Componetes worker
<br> 

- <span style="color:#e28743">**Container runtime**</span>  
Kubernetes no tiene la capacidad de crear contenedores, para ello se apoya de los runtimes instalados en los nodos. Existen diferentes runtimes actualemnte soportados por kubernetes como son:
  - [docker](https://www.docker.com/)
  - [cri-o](https://cri-o.io/)
  - [containerd](https://containerd.io/)
  - [frakti](https://github.com/kubernetes/frakti#frakti) 
- <span style="color:#e28743">**Node Agent (Kubelet)**</span>  
El kubelet es el agente encargado de recibir las definiciones de los pods a crear provenientes principamente del <span style="color:#e28743">Api-Server</span> e interactuar con el <span style="color:#e28743">Container runtime</span>. 
También monitorea la salud y los recursos de los pods en el nodo.  
Para que el kubelet se pueda comunicar con el runtime, debe existir un **CRI-shim**, que no es más que una interfaz para poder comunicar ambos componentes.
- <span style="color:#e28743">**Proxy (kube-proxy)**</span>  
Es el agente encargado de mantener actulizadas las reglas dinámicas de red así como las reglas de red en el nodo. Es responsable de las conecciones TCP, UDP y SCTP a través de un set de pods e implementa las reglas de reenvio definidas por usuarios a través de los objetos de Service Api.
- <span style="color:#e28743">**Addons for DNS, Dashboard, Monitoring, Logging**</span>  
Como tal no representa una implementación en el cluster si no la posibilidad de integrar soluciones de terceros que nos permitan solucionar problemas de monitoreo o logging centralizado a nivel del cluster así como el manejo del DNS o la instalación de un dashboard para la administración del cluster. 


Referencias:
---
- https://kubernetes.io/es/docs/concepts/overview/components/
- https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS158x+3T2020/home
# Operadores

También con conocidos como "watch-loops" o "controllers", constantemente leen el estado del cluster y lo comparan con las especificaciones del mismo.
Existen varios operadores por default, pero se pueden crean operadores propios.

Algunos operadores son:
- [Namespace](./Namespace)
- [Endpoints](./Endpoints)
- [ServiceAccounts](./ServiceAccounts)
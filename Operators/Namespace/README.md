# Namespaces
Los namescpaces ayudan a craer un scope para los recursos en el cluster.

De esta forma podemos restringir el acceso a los recursos por medio de técnicas como RBAC.

Un cluster nuevo puede tener 4 namespaces por default.

```bash
$ kubectl get ns       
NAME              STATUS   AGE
default           Active   41h
kube-system       Active   41h
kube-public       Active   41h
kube-node-lease   Active   41h
```

- **kube-system**: Pods creados para el entorno de kubernetes
- **kube-public**: Pods creados para estar disponibles en todo el cluster 
- **kube-node-lease**: Ayuda a tener en cuenta el heartbeat de los nodos y los pods
- **default**: Espacio para los pods sin namespace específico.

---
## ⚒ Comandos útiles.
<br>

> Obtener los namespaces
```
$ kubectl get namespaces
```


> Crear un namespace
```
$ kubectl create namespace <namespaceName>
```

## Referencias
---
- https://www.youtube.com/watch?v=plB3kyZLHe8&ab_channel=GoogleCloudTech
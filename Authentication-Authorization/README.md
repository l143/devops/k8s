# Autenticación, Authorización
Dado que para poder manejar los recursos dentro del cluster, se hace por medio del ApiServer, existe una forma de poder controlar el acceso a traves de dicho recurso.

> Recordatorio  

| Conceptto | Descripción|
|--|--|
|Authentication|Verifica el "inicio" de sesión|
|Authorization|Verifica el que el usuario pueda tener acceso al recurso deseado|

---
## Tipos de usuarios
<br>

Se peuden entendor dos tipos de usuarios:
- Usuarios normales  
  Este tipo interactua con el cluster fuera del mismo.
- Service accounts  
  Este tipo permite que procesos dentro del cluster puedan interactuar con los recursos dentro del cluster a través del ApiServer



---
## Autenticación
<br>

Para el proceso de Autenticación, existen diversos módulos que permite usar el cluster.

- **X509 Client Certificates**  
  *Esto permite que por medio de un certificado, nos podamos autenticar con el Api Server.*
- **Static Token File**  
  *Nos permite pasarle un archivo de bearer tokens predefinidos al Api Server, estos tokens durarán hasta que se reinice el cluster.*
- **Bootstrap Tokens**  
  *Estios tokens son usados para crear un nuevo cluster*
- **Service Account Tokens**  
  *Estos tokens pueden ser adjuntados a un pod por medio del **ServiceAccount Admission Controller** que permiten a procesos dentro del cluster comunicarse con el Api Server.*
- **OpenID Connect Tokens**  
  *Nos permite conectarnos a proveedores de OAUTH2.*
- **Webhook Token Authentication**  
  *Los bearer tokens pueden ser descargados a un servicio remoto.*
- **Authenticating Proxy**  
  *Permite agregar lógica adicional al proceso de login.*

  ---
## Autorización
<br>

Después de una autenticación exitosa, se valida si el usuario tiene o no permisos para la acción que desea llevar a acabo. Algunas se las cosas que se revisan pueden ser *usuario, grupo, recurso, namespace, etc...*

Al igual que en la autenticación, se tienen varios módulos o *autorizadores*.

Algunos de los móulos de autorización son los siguientes.

- **Node**  
  *Es un modo especial usado por el kubelet para poder leer operaciones de servicos, endpoints, o nodos y escribir operaciones para los nodos, pods y eventos.*  
- **Attribute-Based Access Control (ABAC)**  
  *Es una especificación que combina **políticas** y **permisos***.  Para usar este tipo de debe iniciar el Api Server con --authorization-mode=ABAC y especificar un archivo con la especificación de permisos.    
- **Webhook**  
  *Este tipo permite delegar las desiciones de autorización a un servicio tercero. Para habilitarlo hay que iniciar el Api Server con --authorization-webhook-config-file=SOME_FILENAME*
- **Role-Based Access Control (RBAC)**   
  *Nos permite crear **Roles** con las acciones que podrá hacer un usuario, sin embargo para poder ligarlo hay que crear un **Rolebinding**, lo mismo  funciona para pods con los **ClusterRoles** y los **ClusterRoleBindings**. Para habilitarlo se inicia el APi Server con --authorization-mode=RBAC*

## Referencias
---
 - https://kubernetes.io/docs/reference/access-authn-authz/abac/
 - https://kubernetes.io/docs/reference/access-authn-authz/webhook/
 - https://kubernetes.io/docs/reference/access-authn-authz/rbac/
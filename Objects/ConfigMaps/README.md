# ConfigMaps

Los config maps son pares de **llave**-**valor** que nos ayudan a desacoplar las configuraciones de los pods, manteniendolas en Objectos por separados permitiendo que se compartan y tengan una mejor organización.


---
## ⚒ Comandos útiles.
<br>


> Creación de forma *literal*.
```
$ kubectl create configmap <configmapName> --from-literal=<key>=<val> --from-literal=<key>=<val>
```

> Creación a partir de un *archivo* de configuración.
```
$ kubectl create -f configmap.yml
```

> Creación a partir de un *archivo* de propiedades.
```
$ kubectl create config <configName> --from-file=config.properties
```


> Inspección del configmap
```
$ kubectl get configmaps <configmapName> -o yaml
```


Una vez creados los configMaps se pueden usar dentro de los pods.

> Pasando todo como variables de entorno
```
containers:
  - name: myapp-full-container
    image: myapp
    envFrom:
    - configMapRef:
      name: full-config-map
```

> Pasando solo partes específicas

```
containers:
  - name: myapp-specific-container
    image: myapp
    env:
    - name: SPECIFIC_ENV_VAR1
      valueFrom:
        configMapKeyRef:
          name: config-map-1
          key: SPECIFIC_DATA
```

> Montando como volúmens

```
containers:
  - name: myapp-vol-container
    image: myapp
    volumeMounts:
    - name: config-volume
      mountPath: /etc/config
  volumes:
  - name: config-volume
    configMap:
      name: vol-config-map
```
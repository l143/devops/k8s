# Secrets

Lops secrets son pensados para ocultar cierta información sensible como contraseñas y abstraerlos de forma parecia a los configMaps.

***Los secrets *codifican*, peor no *encrtiptan* la información y la guardan como texto plano dentro del Data Storage(etcd).** Por dicha razón los administradores deben de liomitar el acceso a etcd y el API server. Se puede configurar también al API server para que encripte los secrets en etcd.

---
## ⚒ Comandos útiles.
<br>


> Creación de forma *literal*.
```
$ kubectl create secret generic <secretName> --from-literal=<key>=<val> --from-literal=<key>=<val>
```

> Creación a partir de un *archivo* de configuración.  
*Deberán de escribirse los valores en base64*
```
$ kubectl create -f secret.yml
```


> Creación a partir de un *archivo*.
```
$ kubectl create secret generic <secretName> --from-file=password.txt --from-file=user.txt
```

> Inspección de secrets
```
$ kubectl describe secret <secretName> -o json
```

> Descripción detallada
```
$ kubectl get secret <secretName> -o json | jq -r .data.password | base64 -d
```

Para usarlos dentro de lospods, es muy similar a los configMaps

```
containers:
  - image: wordpress:4.7.3-apache
    name: wordpress
    env:
    - name: WORDPRESS_DB_PASSWORD
      valueFrom:
        secretKeyRef:
          name: my-password
          key: password
```


```
spec:
  containers:
  - image: wordpress:4.7.3-apache
    name: wordpress
    volumeMounts:
    - name: secret-volume
      mountPath: "/etc/secret-data"
      readOnly: true
  volumes:
  - name: secret-volume
    secret:
      secretName: my-password
```
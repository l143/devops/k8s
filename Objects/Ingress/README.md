# Ingresss

*"An Ingress is a collection of rules that allow inbound connections to reach the cluster Services."*


![ingress](../../res/ingress.png)


Para permitir el tráfico al cluster, in IOngress configura un Balanceador de cargas Http/Https para los servicios y provee lo siguiente.
 - TLS(Transport Layer Secutiry)
 - Virtual hosts basados en nombre
 - Balanceador de cargas
 - Reglas customizadas


## Ingress controller
Es una aplicación que observa el nodo master sel server API y actualiza el balanceador de acuerdo a los recursos solicitados.

Los Ingress contollers son conocidos también como *Controlles*, *Ingress Proxy*, *Service Proxy*, *Reverse Proxy*, etc. 

Existen Controllers ya desarollados, siendo los más comunes:
 - [GCE L7 Load Balancer Controller](https://github.com/kubernetes/ingress-gce/blob/master/README.md)
 - [Nginx Ingress Controller](https://github.com/kubernetes/ingress-nginx/blob/main/README.md)

Otras alternativas pueden ser:
 - [Contour](https://projectcontour.io/)
 - [HAProxy](https://haproxy-ingress.github.io/)
 - [Istio](https://istio.io/latest/docs/tasks/traffic-management/ingress/)
 - [Kong](https://konghq.com/)
 - [traefik](https://traefik.io/)
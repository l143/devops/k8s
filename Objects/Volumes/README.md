# Volumes  

Los volumenes es la forma de tener persistencia de datos a pesar de la volatilidad inherente de los contenedores y pods.

Existen principalmente dos tipos.
- Persistent volume
- Persistent volume claim

Un volúmen básicamente es un punto de montaje en el sistema de archivos del engine de contenedores.

Dentro de kubernetes, el Volume es montado en el pod y se comparte con todos los contenedores dentro de este.

---
## 🗃 Tipos de volúmenes.
<br>

### **emptyDir**  
- Se crea un volúmen vacío al iniciar el pod, sin embargo es efímero.  

### **hostPath**
- Permite compartir un directorio entre el **host** y el Pod, si el pod es eliminado, los datos persiste.
- No se recomiendan por temas de seguridad.
- En caso de usarlos, deberán restringirse solo a los datos necesarios y en modo de solo lectura.
- Inconsistencia al tener varios nodos.

Existen diferentes tipos de hostPath   

|Tipo| Comportamiento|
|--|--|
|File| Debe existir el archivo|
|FileOrCreate|Si no existe el archivo, se creará con los permisos 0644 con el mismo owner y group que el kubelet|
|Directory| Debe existir previamente el path|
|DirectoryOrCreate|Si no existe el path, se creará con los permisos 0755 con el owner y group del kubelet.|
|Socket|Debe existir un socker de UNIX en el path|
|CharDevice|Debe existir un dispositivo char en el path|
|BlockDevice|Debe existir un dispositivo block en el path|

### **gcePersistentDisk**
- Este tipo nos permite montar un disco ([PD](https://cloud.google.com/compute/docs/disks)) previamente creado en la nube de goole (GCE)  a nuestro pod.
- El nodo dónde vive el pod, debe correr sobre una máquina virtual en GCE y debe estar en el mismo proyecto y zona que el disco.
- No permite escrituras simultaneas
- Permite lecturas y montajes simultaneos


### **awsElasticBlockStore**

- Este tipo nos permite montar un disco previamente creado en la nube de AWS ([EBS](https://aws.amazon.com/es/ebs/)) a nuestro pod.
- Los pods que usen este tipo, deben de crearse en nodos, deben ser instancias de EC2 y estar en la misma región y zona de disponibilidadque el EBS Volume.
- un EBS, solo se pueden montar en una sola instancia de EC2

### **azureDisk**
- Este tipo nos permite montar un disco previamente creado en la nube de azure a nuestro pod.
- Es común usar un [plugin](https://github.com/kubernetes/examples/blob/master/staging/volumes/azure_disk/README.md) para su uso.

### **cephfs**

- Permite montar un volumen CephFS ya creado.
- Este tipo de volumen permite varios *writers* simultaneos.
- Rquiere un servidor Ceph.


### **NFS**
- Permite montar un volumen NFS ya creado.
- Este tipo de volumen permite varios *writers* simultaneos.
- Rquiere un servidor NFS.

### **Secrets**
- Permite pasarle información sensible al Pod, ocmo contraseñas.

### **configMap**
- Este tipo permite pasar configuración o comandos con sus argumentos al Pod.

---
## 📥 Modos de acceso.
<br>  

|Modo|Abreviación|Descripción|  
|--|--|--|
|ReadWriteOnce|RWO|El volúmen puede ser montado para **escritura** y **lectura** por **un solo nodo**|
|ReadOnlyMany|ROX|El volúmen puede ser montado para **lectura** por varios nodos|
|ReadWriteMany|RWX|El volúmen pouede ser motado para **escritura** y **lectura** por varios nodos.|
|ReadWriteOncePod|RWOP|El volúmen puede ser montado como **lectura** y **escritura** para un solo pod. (Aplica en verisones posteriores de kubernetes a las 1.22 y para CSI volumes)|
---
## 🗄 Persistent volumes.
<br>

Este rescurso es provisionado por el administrador del clúster usando alguno de los tipos listados anteriormente.

---
## 🗄 Persistent volumes Claims.
<br>

Este recurso es utilizado por los usuarios y funciona para montar en un pod un volúmen en base al tipo, modo de acceso y tamaño especificados en el claim.

![pvc2](../../res/pvc2.png)


---
## 🔌 Container Sotorage Interface (CSI).
<br>

Es una especificación que se utiliza por los proveedores de storage y la comunidad que desarrolla los orquestadores paea estadarizar las interfaces de Volúmenes.

## Referencias
- https://kubernetes.io/docs/concepts/storage/volumes/
- https://kubernetes.io/docs/concepts/storage/persistent-volumes/#types-of-persistent-volumes
https://kubernetes.io/docs/concepts/storage/persistent-volumes/#access-modes
- https://kubernetes.io/docs/concepts/storage/volumes/#csi
# Objetos de kubernetes

Representan entidades persistentes dentro del cluster que describen:

 - Las aplicaciones contenerizadas corriendo en el cluster
 - Los nodos dónde corren esas aplicaciones 
 - Las políticas de fallo, reinicio y actualización asociadas a las aplicaciones.


 Los objectos que podemos crear son:
  - [Labels & Selectors](./Labels&Selectors)
  - [Pods](./Pod)
  - [Replicasets](./Replicaset)
  - [Services](./Service)
  - [Liveness](./Liveness)
  - [Readiness](./Readiness)
  - [Volumes](./Volumes)
  - [ConfigMaps](./ConfigMaps)
  - [Secrets](./Secrets)
  - [Ingress](./Ingress)

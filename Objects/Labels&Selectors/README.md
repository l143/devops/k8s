# Labels

Los labels son pares de **llave**-**valor** que se le puden asignar a otros objectos como Pods, Namespaces, Replicasets, Nodos ó Volúmenes.

- Varios objetos puede compartir los labels.
- Los controladores pueden usar los labels para agrupar logicamente objetos, en lugar de usar nombre o ID's.


# Selectors
Los selectores son usados para filtrar un subconjunto de recursos.
Existen dos tipos de selectores soportados.

- **==** *aka* **=, !=**  
  *Representan igualdad o diferencia.*

- **in, notin, !**  
  *Representan un set, por ejemplo **in(dev,qa)** seleccionará cualquiera que tenga en label dev o qa. **!app** Seleccionará todo lo que no contenga el label app*
# Deployment

Internamente permiten ejecutar un [Replicaset](../Replicaset/README.md) aunado a una **Revision** que posteriormente nos permitirá hacer un rollback.

El deployment creara una nueva **Revision** solo en los siguientes casos:
 - Actualización de la **imagen** en el contenedor
 - Actualización del **puerto** en el contenedor
 - Actualización de los **volúmens** y **montajes**

*El escalamiento o la aplicación de labels no crean una nueva **Revision*** 

El siguiente diagrama ejempplifica el cambio de una **Revision** y un nuevo [Replicaset](../Replicaset/README.md) debido al cambio en la imagen del contenedor.

![DeployemtUpdate](../../res/ReplikaSet_B.png)


---
## ⚒ Comandos útiles.
<br>

> Aplicar un deployment
```
$ kubectl apply -f <deploymentFile.yml>
```

> Listar los deployments
```
$ kubectl get deploy
```

> Revisar el historial de resvisiones de un deployment
```
$ kubectl rollout history deploy <deploymentName>
```

> Revisar el historial de una revisión por medio del id
```
$ kubectl rollout history deploy <deploymentName> --revision=<revisionID>
```

> Actualizar la imagen de un deployment
```
$ kubectl set image deployment <deplymentName> <containerName>=<imageName>
```

> Hacer un rollback de un deployment
```
$ kubectl rollout undo deployment <deploymentName> --to-revision=1
```
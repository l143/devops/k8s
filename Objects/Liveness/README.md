# Liveness Probe

Para que el cluster pueda darse cuenta de que los pods están en un estado no óptimo y decida reiniciarlos, se necesita implementar un **liveness probe** dentro del pod.

Dicho liveness, no es más que una acción que se realizará para saber si se debe o no reiniciar el pod.

Existen 3 tipos
- Liveness command
- Liveness HTTP request
- TCP Liveness probe.

---

## Liveness command

Ejecuta un comando dentro del pod, si el comando no tiene una salida exitosa, reiniciará el pod.

```yaml
livenessProbe: 
  exec:
    command:
      - cat
      - /usr/share/nginx/html/index.html
```

---

## Http Request

Si la petición regresa una respuesta diferente de ok(200), se reiniciará el pod.

```yaml
livenessProbe: 
  httpGet:
    path: /healthz
      port: 8080
      httpHeaders:
      - name: X-Custom-Header
        value: Awesome
```
---

## Tcp

El kubelet tratará de abrir un socket tcp en el contenedor, en caso de que no pueda reiniciará el mismo.
```yaml
livenessProbe: 
  tcpSocket:
    port: 8080
```

---

Aparte del *exec*, *httpGet* ó *tcpSocket*, se pueden configurar algunos otros parámetros en livenessProbe.

> *Indica el tiempo que debe pasar al iniciar el pod para ejecutar por primera vez la acción.*
```yaml
initialDelaySeconds: 3
```

> *Indica cuanta veces tendrá permitido fallar la acción para reiniciar el pod*
```yaml
failureThreshold: 1
```
> *Indica cada cuánto se hará la acción de comprobación.*
```yaml
periodSeconds: 5
```
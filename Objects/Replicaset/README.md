# Replicaset

El replicaset nos permite escalar el número de pods corriendo de una imagen específica.   

El control manager revisará constantemente que el estado deseado sea igual al estado en el cluster con respecto al númerop de pods.

![Replicasets](../../res/Replica_Set1.png)

---
## ⚒ Comandos útiles.
<br>

> Aplicar un replicaset
```
$ kubectl apply -f <replicaFile.yml>
```

> Listar los replicasets
```
$ kubectl get rs
```

> Ver el estado de una replica
```
$ kubectl describe rs <replicasetName>
```


## Referencias
---
- https://kubernetes.io/es/docs/concepts/workloads/controllers/replicaset/
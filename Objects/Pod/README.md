# POD

El pod es la únidad mínima de aplicacion que se puede crear en un cluster.

Resuelve el problema del control en los puertos para el nodo worker, ya que cuando se crea un pod, tiene su proipio namespace de red y su propia conexión virtual de ethernet.

- Un pod puede tener más de un contenedor.
- Los contenedores dentro del pod comparten la IP del mismo.
- Se pueden tener hasta 6 copntenedores en un pod.
- Los pods deben considerarse como efímeros, ya que otros objetoc somo los deployent permiten crearlo y destruirlos.

![pods](../../res/pods-1-2-3-4.svg)

---
## ⚒ Comandos útiles.
<br>

> Crear un pod a partir de un yml
```
$ kubectl apply -f <podfile.yml>
```

> Ver información del pod
```
$ kubectl describe pod <podName>
```

> Ejecutar algo en el pod
```
kubectl exec -it <podName> -- <command>
```

## Referencias
---
- https://www.youtube.com/watch?v=5cNrTU6o3Fw&t=675s&ab_channel=TechWorldwithNana
# Services

Dado que los pods son creados y eliminados de forma constantre en el cluster, se requiren de otro tipo de objecot para poder comunicarnos con los servicios que exponen.

Para esto, nos ayudamos de los [labels & selectors](../Labels&Selectors/README.md).

Los servicios pueden exponer diversos objectos como los Pods, Replicasets, Deployments, DaemonSets, y StatefulSets, sin embargo el realmente de encargado de hacer las modificaciones dentro de las iptables de cada nodo del cluster.

![kubeproxy](../../res/kubeproxy.png)

---
## ⚒ Tipos de servicios.
<br>

> ClusterIp default  

Este servicio solo es funcional dentro del cluster. Recibe una IP virtual que es usada para redireccionar el tráfico a los pods por medio de los labels.

> NodePort  

Toma un puerto en el rango 30000-32767, mismo que será reservado en cada nodo del cluster para poder redirigir el tráfico a los pods correspondientes.

Este servicio se puede usar para tener conexión fueral del cluster.


> Load balancer  

Con este tipo de servicio, se cranlos dos anteriores, sin embargo en un paso adicional, se apoya de la funcionalidad del provedor de nube para crear un balanceador de cargas.

> External name  

Es un servicio especial que no define ningún endpoint sino que regresa un registro CNAME de un servicio externo. Su uso principal es para poder usar servicios externos dentro del cluster.



---
## ⚒ Comandos útiles.
<br>


> Crear un servicio a partir de un yml
```
$ kubectl apply -f <service.yml>
```

> Ver información del servicio
```
$ kubectl describe service <serviceName>
```


> Ejecutar un contenedor de pruebas para hacer curl

```
$ kubectl run curl-test --image=radial/busyboxplus:curl -i --tty --rm

```
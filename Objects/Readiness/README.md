# Readiness Probe

Son muy parecidos a los liveness pero estos se basan en poder leer un archivo para que el pod pueda recibir peticiones.

Son utiles en ocaciones dónde el pod inicia pero depende de servicios de terceros o cargar configuración al inicio.

```yaml
readinessProbe: 
  exec:
    command:
      - cat
      - /usr/share/nginx/html/index.html
  initialDelaySeconds: 5
  periodSeconds: 5
```

> *Los liveness probe no dependen de los readiness, así que si se usan ambos deberán considerarse los tiempos.*

# Kubernetes 
*K8s*

![k8s](./res/k8s.jpeg)

Kubernetes es una plataforma portable y extensible de código abierto para administrar cargas de trabajo y servicios. 

- [Arquitectura](./Architecture.md)
- [Kubectl](./Kubectl.md)
- [Objetos](./Objects/README.md)
- [Operadores](./Operators/README.md)


Referencias:
---
- https://kubernetes.io/es/docs/concepts/overview/what-is-kubernetes/
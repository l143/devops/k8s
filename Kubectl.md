# Kubectl

***Cli***ente de terminal oficial para comunicarnos con los clusters de kubernetes.  

---
## 🧰 Instalación en linux
Bajamos el último ejecutable estable.
```bash
$ curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
```

Agregamos permisos de ejecución.
```bash
$ chmod +x ./kubectl
```
Movemos a algún direcotrio del PATH
``` bash
$ sudo mv ./kubectl /usr/local/bin/kubectl
```

> *Nota*: kubectl utiliza la variable de entorno **KUBECONFIG** para poder localizar los archivos *config* de los clusters a los cuales se podra conectar. En caso de que no encuentre esa variable, intentará leer el archivo en la ubicación **$HOME/.kube/config**


---
## ⚒ Comandos útiles.
<br>

> Conectarnos a un cluster diferente al default
```
$ kubectl --kubeconfig <configFilePath>
```

> Obtener la información general del cluster
```
$ kubectl cluster-info
```

> Listar los nodos del cluster
```
$ kubectl get nodes
```

> Ver la referencia sobre los recursos a administrar
```
$ kubectl explain <clusterResouce>.<resourceDetail>
```
---
## ⚙ Config.
<br>

> Ver configuración del kubectl

```
$ kubectl config view
```


Referencias
---
- https://kubernetes.io/es/docs/tasks/tools/install-kubectl/
- https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands
- https://www.youtube.com/watch?v=VSzZK8HmI08&ab_channel=PeladoNerd